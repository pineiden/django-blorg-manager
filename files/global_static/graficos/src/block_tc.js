import {BaseChart} from './base_chart';
let d3 = require("d3");

function constant(value, default_value){
    if (default_value){
        return default_value;
    }else{
        return 0;
    }
}

class BlockChart extends BaseChart{
    /*
      The panchomon chart for time leap cases
    */
    constructor(parameter, options){
        super("block", parameter, options);
    }


    update(pre_data){
        if (this.initial){
            this.init();
            this.initial=false;
        }
        let opts = this.opts;
        let id_css = this.id_css;
        let tooltip = this.tooltip;
        let hm_format = this.hm_format;
        // svg eleemnt
        let data = pre_data.filter(d=>d.time_connected>=0);
        // min and max time
        let time_extent = d3.extent(data, d=> new Date(d.dt_gen));

        var x_scale = d3.scaleTime().range([0, this.width-this.margin.right]);
        var y_scale = d3.scaleLinear().range([this.height,this.margin.top]);

        // define ejes
        var x_axis = d3.axisBottom(x_scale);
        var y_axis = d3.axisLeft(y_scale).ticks(1);
        x_axis.tickSize(3);
        y_axis.tickSize(3);

        x_scale.domain(time_extent);
        y_scale.domain([0, d3.max(data, function(d){return d.time_connected;})]);

        let axis_element=this.svg.append("g")
            .attr("class", "x axis")
            .attr("transform", `translate(0,${this.height})`)
            .call(x_axis);
        axis_element.selectAll("text")
            .style("text-anchor","end")
            .attr("dx", "-.5em")
            .attr("dy","-.5em")
            .attr("transform", "rotate(-60)");


        let color=this.color();

    let save_height = 0;
    let save_color = "white";
    this.svg.append("g")
        .selectAll("rect")
        .attr("stroke","#F4F4F" )
        .data(data)
        .join("rect")
        .attr("fill", function(d,i){
            let tc = d.time_connected<=30?d.time_connected:30;
            let this_color = color(tc);
            return this_color;})
        .attr("width", function(d,i){
            this.index=i;
            let end = new Date(d.dt_gen);
            let init = 
                i-1>=0?new Date(data[i-1].dt_gen):new Date(data[0].dt_gen);
            //console.log("Init", init,"End", end);
            this.ref=init;
            let width = x_scale(end)-x_scale(init);
            return width;
                              })
            .attr("height", function(d,i){
            return opts.block_height;
        })
        .attr("x",function(d,i){
            let dato = i-1>=0?data[i-1].dt_gen:data[0].dt_gen;
            return x_scale(new Date(dato));})
        .attr("y",function(d,y){
            let h=axis_element.attr("height");
            let height=d3.select(this).attr("height");
            return height;})
        .on("click", function(event, d){
            const e = this.svg.nodes();
            const i = e.indexOf(this);
        })
        .on("mouseover", function(event, d,i){
            tooltip.transition().duration(200).style("opacity", .9);
            let dt =  new Date(d.dt_gen);
            var i = this.index;
            let text_p = `<div class="tp">
                        <h3 class="name">${d.code}</h3>
                        <div class="info">Info</div>
                        <div class="dt">DT: ${hm_format(dt)}</div>
                        <div class="tc">TC: ${d.time_connected.toFixed(2)}`
                + ` ${opts.medida}</div></div>`;
            tooltip.html(text_p); 
            save_height=d3.select(this).attr("height");
            save_color=d3.select(this).attr("fill");
            let new_height=(parseInt(save_height) + 10);
            d3.select(this).attr('height',new_height).attr("fill",opts.selected_color);
            let y_pos=parseInt(d3.select(this).attr("y"));
            let svg_offset=document.getElementById(id_css).offsetTop;
            return tooltip.style("visibility", "visible").style("top",
                                                                (y_pos+svg_offset-30)+"px");
        })
        .on("mousemove", function(event, d,i){
            let new_height=(parseInt(save_height) + 10);
            d3.select(this).attr('height',new_height).attr("fill",opts.selected_color);
            let y_pos=parseInt(d3.select(this).attr("y"));
            let svg_offset=document.getElementById(id_css).offsetTop;
            return tooltip.style("left",(event.clientX+10)+"px").style("top",
                                                                       (y_pos+svg_offset-30)+"px");})
        .on("mouseout", function(d,i){
            d3.select(this).attr('height',save_height).attr("fill",save_color);
            tooltip.text(d); return tooltip.style("visibility", "hidden");
        });
    }
}



export {BlockChart}
