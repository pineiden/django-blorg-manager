/*
Sources:

https://bl.ocks.org/robyngit/89327a78e22d138cff19c6de7288c1cf
http://mcaule.github.io/d3-timeseries/
 */

let d3 = require("d3");
import { readData, print } from "./functions";



import {TimeSerie} from './timeserie';
import {BlockChart} from './block_tc';
import {Histogram} from './histogram';

import { interval, from, empty } from 'rxjs';
import { map, mapTo, mergeMap,switchMap, catchError, repeat } from 'rxjs/operators';
import {w3c_websocket_client} from './ws_client';


function create_chart(elem, parameter, params){
    const charts = {
        "timeserie":TimeSerie,
        "block":BlockChart,
        "histogram":Histogram
    };
    let chart = new charts[elem](parameter, params);
    return chart;
}

function get_settings(elem, station, field, opts_charts){
    let default_settings = {
        "timeserie":{},
        "block":{label_y:"", height:100, y_ticks:5},
        "histogram":{height:250, y_ticks:5, label_y: "Cantidad"}
    };
    let select = default_settings[field];
    let new_settings = opts_charts[field];
    Object.assign(select, {station:station, value:elem}, new_settings);
    return select;
}

function createArrayTime(inicio,final,step){
    if(inicio>=final){
        final=inicio;
    }
    let array = [];
    if (inicio>=final-step){
        return [[inicio, final]];
    }
    for(let time=inicio;time<=final;time+=step){
        let this_final = time+step;
        if (this_final>=final){
            this_final=final;
        };
        let pair = [time, this_final];
        array.push(pair);
    }
    return array;
}

export function load_charts(idname){
    let text = document.getElementById(idname).innerHTML;
    let data = JSON.parse(text);
    let base_url = data.base_url;
    let inicio = data.inicio;
    let final = data.final;
    let station = data.station;
    let opts_charts = data.charts;
    let charts = {"timeserie":1, "block":1, "histogram":1};
    /*acuumulate jsons*/
    const step = 1200; // 1 hour
    let all_data=[];
    let array_time=createArrayTime(inicio,final,step);
    // turn array in observable
    const auth_token = document.getElementById('auth_token').innerHTML;
    const observer ={
        next: dataset => {
            dataset.sort(function(a,b){
                var da = new Date(a.dt_gen),
                    db = new Date(b.dt_gen);
                if(da<db) return -1;
                if (da>db) return 1;
                return 0;
            });
            dataset.forEach(elem=>all_data.push(elem));
            },
        error: err => console.log("Error", err),
        complete: () => {
            console.log("Complete data", all_data.length);
            all_data.sort(function(a,b){
                var da = new Date(a.dt_gen),
                    db = new Date(b.dt_gen);
                if(da<db) return -1;
                if (da>db) return 1;
                return 0;
            });
            Object.entries(opts_charts).forEach( ([elem, opts]) =>{
                let fields =  Object.keys(opts).filter(elem=>elem in charts);
                fields.forEach(field=>{
                    let params = get_settings(elem, station, field, opts);
                    let chart = create_chart(field, elem, params);
                    chart.update(all_data);
                });
            });

        }
    };

    const source$ = from(array_time);
    source$.pipe(
        mergeMap(dates=>{
            let inicio_ = dates[0];
            let final_ = dates[1];
            let url=`${base_url}${station}/desde=${inicio_}/hasta=${final_}`;
            let fetch$ = from(d3.json(url, {
                headers: new Headers({
                    "Authorization": `Token ${auth_token}`
                })
            }));
            return fetch$.pipe(
                repeat(1),
                catchError((error, caught)=>{
                    console.log("Error al solicitar datos", error);
                    return caught;
                })
            );
        }),
    ).subscribe(observer);

}



export function ws_load_charts(idname){
    // id log identifica la 'selectionlogquery''
    const id_log = document.getElementById("id_log").innerHTML;
    const path='ws/reporte_unidad';
    let port = location.port ? location.port: '';
    port = port==='1234'?'8000':port;
    const host = location.hostname;
    const schema = location.protocol==='https:' ? 'wss':'ws';
    let uri = `${schema}://${host}:${port}/${path}/id=${id_log}/`;
    console.log("URI ws". uri);
    let text = document.getElementById("graficos").innerHTML;
    let data = JSON.parse(text);
    let charts = {"timeserie":1, "block":1, "histogram":1};
    /*connect to websocket*/
    let opts = {data, charts};
    w3c_websocket_client(uri, opts, get_settings, create_chart);

    // // turn array in observable
    // const auth_token = document.getElementById('auth_token').innerHTML;
    // const observer ={
    //     next: dataset => {
    //         dataset.sort(function(a,b){
    //             var da = new Date(a.dt_gen),
    //                 db = new Date(b.dt_gen);
    //             if(da<db) return -1;
    //             if (da>db) return 1;
    //             return 0;
    //         });
    //         dataset.forEach(elem=>all_data.push(elem));
    //         },
    //     error: err => console.log("Error", err),
    //     complete: () => {
    //         console.log("Complete data", all_data.length);
    //         all_data.sort(function(a,b){
    //             var da = new Date(a.dt_gen),
    //                 db = new Date(b.dt_gen);
    //             if(da<db) return -1;
    //             if (da>db) return 1;
    //             return 0;
    //         });
    //         Object.entries(opts_charts).forEach( ([elem, opts]) =>{
    //             let fields =  Object.keys(opts).filter(elem=>elem in charts);
    //             fields.forEach(field=>{
    //                 let params = get_settings(elem, station, field, opts);
    //                 let chart = create_chart(field, elem, params);
    //                 chart.update(all_data);
    //             });
    //         });

    //     }
    // };

    // const source$ = from(array_time);
    // source$.pipe(
    //     mergeMap(dates=>{
    //         let inicio_ = dates[0];
    //         let final_ = dates[1];
    //         let url=`${base_url}${station}/desde=${inicio_}/hasta=${final_}`;
    //         let fetch$ = from(d3.json(url, {
    //             headers: new Headers({
    //                 "Authorization": `Token ${auth_token}`
    //             })
    //         }));
    //         return fetch$.pipe(
    //             repeat(1),
    //             catchError((error, caught)=>{
    //                 console.log("Error al solicitar datos", error);
    //                 return caught;
    //             })
    //         );
    //     }),
    // ).subscribe(observer);

}
