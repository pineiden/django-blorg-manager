import {webSocket} from 'rxjs/webSocket';
import {map,filter, pluck} from 'rxjs/operators';

/*
https://rxjs-dev.firebaseapp.com/api/webSocket/webSocket

  webSocket: produce un webSocketSubject
  una vez que se subscribe, se puede escuchar  los mensajes que se reciban

  al suscribirse, ocupar los menotods [next, error, complete]

 */
export function w3c_websocket_client(uri, opts, get_settings, create_chart) {
    let data = opts.data;
    //
    let inicio = data.inicio;
    let final = data.final;
    let station = data.station;
    let opts_charts = data.charts;
    //
    let charts  = opts.charts;
    let all_data = [];
    //
    let client = webSocket(
        {
        url:uri,
        openObserver: {
            next: ()=>{
                console.log(`Websocket on ${uri} opened`);
                /*Init charts*/
            }
        },
        closeObserver :{
            next: ()=>{
                console.log(`Websocket on ${uri} closed`);
                console.log("Closing websocket");}
        }
        });

    client.next({accion:"send", args:[]});

    client.pipe()
     .subscribe({
         next: data_json =>{
             /*Update charts with new data*/
             if (data_json.valor){
                 data_json.valor["code"] = station;
                 all_data.push(data_json.valor);
             }
             if (data_json.END) {
                 Object.entries(opts_charts).forEach( ([elem, opts]) =>{
                     let fields =  Object.keys(opts).filter(elem=>elem in charts);
                     fields.forEach(field=>{
                         let params = get_settings(elem, station, field, opts);
                         let chart = create_chart(field, elem, params);
                         chart.update(all_data);
                     });
                 });   
             }
        },
        error: (e) => console.log('Connection Error',e),
         complete : () => {
             console.log("Completado ws");
         }
     });

}
