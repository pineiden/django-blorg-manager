let d3 = require("d3");
import {BaseChart} from './base_chart';


class TimeSerie extends BaseChart{
    constructor(parameter,  options){
        let blue_line= "#6495ED";
        let pre_options = {
            line_color:blue_line,
            value:"time_connected",
            time_parameter:"dt_gen"
        };
        Object.assign(pre_options, options);
        super("timeserie",parameter, pre_options);
    }
    line(opts,  x_scale, y_scale){
        let line =  d3.line()
            .x(d=>x_scale(new Date(d[opts.time_parameter])))
            .y(d=>y_scale(d[opts.value]))
            .curve(d3.curveCatmullRom.alpha(0.5));
        return line;
    }
    update(pre_data, options){
        if (this.initial){
            this.init();
            this.initial=false;
        }
        let opts = this.opts;
        let height=this.height;
        let width=this.width;
        let margin=this.margin;

        var x_scale = d3.scaleTime().range([0, width-margin.right]);
        var y_scale = d3.scaleLinear().range([height,margin.top]);
        // define ejes
        var x_axis = d3.axisBottom(x_scale);
        var y_axis = d3.axisLeft(y_scale).ticks(10);
        x_axis.tickSize(3);
        y_axis.tickSize(3);
        // load data
        let data = pre_data.filter(d=>d[opts.value]>=0);
        // dominios de escale para x e y
        let time_extent = d3.extent(data, d=> new Date(d[opts.time_parameter]));
        x_scale.domain(
            time_extent);
        y_scale.domain(
            [0, d3.max(data, function(d){return d[opts.value];})]);

        // añadir ejes a svg
        let color = this.color();
        let id_css = this.id_css;
        let tooltip = this.tooltip;
        let hm_format = this.hm_format;
        let l = data.length;
        let line =  this.line(opts, x_scale, y_scale);
        this.svg.append("g")
            .attr("class", "x axis")
            .attr("transform", `translate(0,${height})`)
            .call(x_axis)
            .selectAll("text")
            .style("text-anchor","end")
            .attr("dx", "-.8em")
            .attr("dy","-.55em")
            .attr("transform", "rotate(-60)");

        this.svg.append("g")
            .attr("class","y axis")
            .call(y_axis)
            .append("text")
            .attr("transform", "rotate(-90)")
            .attr("y", 6)
            .attr("dy",".71em")
            .style("text-anchor", "end")
            .text("%d");

        // Define the div for the tooltip

        this.svg.append("path")
            .datum(data)
            .attr("fill","none")
            .attr("stroke",opts.line_color)
            .attr("stroke-width","1px")
            .attr("class","line")
            .attr("d", line);

        // circles every n points
        this.svg.append("g")
            .attr("fill","white")
            .selectAll("circle")
            .data(data)
            .join("circle")
            .attr("stroke",d=>color(d[opts.value]))
            .attr("cx",d=> x_scale(new Date(d[opts.time_parameter])))
            .attr("cy",d=> y_scale(d[opts.value]))
            .attr("r",2.0)
            .on("click", function(event, d){
                const e = this.svg.nodes();
                const i = e.indexOf(this);
            })
            .on("mouseover", function(event, d,i){
                let dt =  new Date(d[opts.time_parameter]);
                let text = `<div class="tp"><h3 class="name">${d.code}</h3>
                            <div class="dt">DT: ${hm_format(dt)}</div>
                            <div class="tc">TC: ${d[opts.value]} [s]</div></div>`;
                d3.select(this).attr('r',3.0).attr("fill","#81DA01");
                let svg_offset=document.getElementById(id_css).offsetTop;
                let cx = d3.select(this).attr("cx");
                let cy = d3.select(this).attr("cy");
                let matrix = this.getScreenCTM()
                    .translate(cx,cy);
                tooltip.html(text);
                tooltip.style("visibility", "visible")
                    .style("left", (window.pageXOffset+matrix.e+15)+"px")
                    .style("top",  (window.pageYOffset+matrix.f-30)+"px");
            })
            .on("mousemove", function(event, d){
                d3.select(this).attr('r',3.0).attr("fill","#81DA01");
                let svg_offset=document.getElementById(id_css).offsetTop;
                let cx = d3.select(this).attr("cx");
                let cy = d3.select(this).attr("cy");
                let matrix = this.getScreenCTM()
                    .translate(cx,cy);
                tooltip.style("visibility", "visible")
                    .style("left", (window.pageXOffset+matrix.e+15)+"px")
                    .style("top",  (window.pageYOffset+matrix.f-30)+"px");
            })
            .on("mouseout", function(d){
                d3.select(this).attr('r',2.0).attr("fill","white");
                tooltip.text(d); return tooltip.style("visibility", "hidden");
            });
    };

}

export {TimeSerie};
