import {BaseChart} from './base_chart';
let d3 = require("d3");


class Histogram extends BaseChart{
    constructor(parameter,  options){
        super("histogram",parameter, options);

    }

    histogram(x_scale,x_ticks){
        let parameter=this.parameter;
    var histo = d3.histogram()
        .value(function(d) { return d[parameter]; })   // I need to give the vector of value
        .domain(x_scale.domain())  // then the domain of the graphic
        .thresholds(x_ticks); // then the numbers of bins
     return histo;
    };

    // axis.
    update(pre_data){
        if (this.initial){
            this.init();
            this.initial=false;
        }
        // call rectables
        let save_color = "white";
        let save_height = 0;
        let amount_time = {};

        /* filter to valid data*/

        let data = pre_data.filter(d=>d[this.parameter]>=0);

        // min and max time
        let tc_extent = d3.extent(data, d=> d[this.parameter]);
        var x_scale = d3.scaleLinear().domain(tc_extent).range([0, this.width]);
        var x_ticks=x_scale.ticks(this.opts.bloques);
        // define ejes
        var x_axis = d3.axisBottom(x_scale);
        // set axis to svg
        let axis_element=this.svg.append("g")
            .attr("class", "x axis")
            .attr("transform", `translate(0,${this.height})`)
            .call(x_axis);
        // calculate bins for histogram
        let histo_calc =  this.histogram(x_scale,x_ticks);
        var bins = histo_calc(data);
        let selected = this.opts.selected_color;
        // y axis

        let y_scale = d3.scaleLinear().range([this.height, 0]);
        let y_ticks=y_scale.ticks(this.opts.y_ticks);

        y_scale.domain([0, d3.max(bins, function(d){
            return d.length;
        })]);

        this.svg.append("g").style("font-size",10).call(d3.axisLeft(y_scale));

        /*integrate all time connected*/
        Object.entries(bins).forEach(([i,bin])=>{
            amount_time[i] =  bin.length>0?bin.map(d=>d[this.parameter]).reduce((a,b)=>a+b):0;
        });
        let color = this.color();
        let opts = this.opts;
        let id_css = this.id_css;
        let tooltip = this.tooltip;
        let hm_format = this.hm_format;
        let height=this.height;
        let width=this.width;
        this.svg.append("g")
            .style("font-size",10)
            .selectAll("rect")
            .data(bins)
            .join("rect")
            .attr("x",1)
            .attr("stroke","#F4F4F" )
            .attr("fill", function(d,i){
                this.index=i;
                let delta =x_scale(d.x1);
                let this_color = color(delta);
                return this_color;})
            .attr("x",function(d,i){
                return x_scale(d.x0);})
            .attr("y",function(d,y){
                return y_scale(d.length);})
            .attr("width", function(d){
                let delta =x_scale(d.x1)-x_scale(d.x0);
                let result=delta?delta-opts.bar_margin:delta;
                return result>=0?result:0;
            })
            .attr("height", function(d){
                return height - y_scale(d.length);
            })
            .on("mouseover", function(event, d){
                let dt =  new Date(d.dt_gen);
                let text = `<div class="tp">
                            <div class="dt">Total: ${amount_time[this.index].toFixed(3)}${opts.medida}</div>
                            <div class="dt">DT: ${d.x0}->${d.x1}${opts.medida}</div>
                            <div class="tc">Cantidad: ${d.length}</div></div>`;
                tooltip.html(text);
                save_height=d3.select(this).attr("height");
                save_color=d3.select(this).attr("fill");
                let new_height=(parseInt(save_height) + 10);
                d3.select(this).attr('height',new_height).attr("fill",selected);
                let svg_offset=document.getElementById(id_css).offsetTop;
                return tooltip.style("visibility", "visible").style("top",
                                                                    (event.clientY-5)+"px");
            })
            .on("mousemove", function(event, d){
                let new_height=(parseInt(save_height) + 10);
                d3.select(this).attr('height',new_height).attr("fill",selected);
                let svg_offset=document.getElementById(id_css).offsetTop;
                return tooltip.style("left",(event.clientX-5)+"px").style("top",
                                                                           (event.clientY+svg_offset)+"px");})
            .on("mouseout", function(d){
                d3.select(this).attr('height',save_height).attr("fill",save_color);
                tooltip.text(d); return tooltip.style("visibility", "hidden");
            });

    };

}



export {Histogram};
