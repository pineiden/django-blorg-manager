let d3 = require("d3");

class BaseChart{
    constructor(name, parameter, options){
        this.parameter = parameter;
        const verde_oscuro = "#15c500fb";
        const verde_claro = "#00ff0cff";
        const amarillo = "#e8ff00fe";
        const naranjo = "#ffd500e7";
        const rojo = "#ff2300ff";
        const purple = "purple";
        this.initial = true;
        this.id_css = name+"_"+parameter;
        this.margin = {top: 10, right: 20, bottom: 50, left: 45};
        this.opts = {
            margin: {top: 10, right: 20, bottom: 50, left: 45},
            width:600,
            height:200,
            bloques:50,
            color:[verde_oscuro, verde_claro, amarillo, naranjo, rojo],
            selected_color:purple,
            color_limits:[0,30],
            bar_margin:2,
            y_ticks:5,
            x_ticks:10,
            medida:"[s]",
            label_y:"Cantidad por tramo [s]",
            label_x:options.station,
            block_height:20
        };
        this.update_options(options);
        this.width = this.opts.width - this.margin.left - this.margin.right;
        this.height = this.opts.height - this.margin.top - this.margin.bottom;
        this.svg_width = this.width + this.margin.left+this.margin.right;
        this.svg_height = this.height+this.margin.top+this.margin.bottom;
        this.svg = d3.select("#"+this.id_css).append("svg")
            .attr("viewBox",
                  [0,0,this.svg_width, this.svg_height])
            .append("g")
            .attr("transform",`translate(${this.margin.left},${this.margin.top})`);
        // formateo de tiempo
        this.formatTime = d3.timeFormat("%e %B");
        this.hm_format = d3.timeFormat("%H:%M:%S");
    }

    update_options(options){
        let checked = [];
        Object.assign(this.opts, options);
    }

    color(){
        let color = d3.scaleQuantize()
            .domain(this.opts.color_limits)
            .range(this.opts.color);
        return color;
    }

    init(){
        /*
          set the axis
         */
        // Define the div for the tooltip

        this.tooltip = d3.select("body")
            .append("div")
            .style("z-index", "10")
            .style("visibility", "hidden")
            .style("position", "absolute")
            .attr("class","tooltip")
            .text("a simple tooltip");
        this.svg.append("text")             
            .attr("transform",
                  "translate(" + (this.width/2) + " ," + 
                  (this.height + this.margin.top + 20) + ")")
            .style("text-anchor", "middle")
            .attr("class","axis_label")
            .text(this.opts.label_x);
        // text label for the y axis
        this.svg.append("text")
            .attr("transform", "rotate(-90)")
            .attr("y", 0 - this.margin.left)
            .attr("x",0 - (this.height / 2))
            .attr("dy", "1em")
            .style("text-anchor", "middle")
            .style("font-size",10)
            .attr("class","axis_label")
            .text(this.opts.label_y);      

    }

    update(pre_data){

    }

}

export {BaseChart}
