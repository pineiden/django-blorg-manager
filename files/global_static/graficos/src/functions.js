let d3 = require("d3");

export function formatData(elem, opts, newData){
    let newJSON={};
      if (typeof elem === "object"){
      Object.entries(elem).forEach(([key, values])=>{
          if (opts.format.hasOwnProperty(key)){
              let callback = opts.format[key];
              if (Array.isArray(values)){
                  newJSON[key]=values.map(callback);}
              else {
                  newJSON[key]=callback(values);
              }
              }
          else{
              newJSON[key]=values;
          }
      });
          newData.push(newJSON);
      }
    else{
        newData.push(elem);
    }
};
export function readData(data_id, opts){
    /*
      data_id :: es el id del trozo de <script> que contiene la adata
      opts :: opts.format el el json con field:callback que aplica al campo una funcion
      Se transforma, necesariamente, aquellos vectores que necesiten un proceso adicional

    */
    let element = document.getElementById(data_id);
    let new_data = [];
    if (element) {
        let data = JSON.parse(element.textContent);
        if (Array.isArray(data)){
            data.forEach(elem=> {
                formatData(elem, opts, new_data);
            });

        }
        else if ( typeof data == 'object' && !Array.isArray(data)){
            formatData(data, opts, new_data);
        }
    }
    return new_data;
}

export function print(data){
    console.log(data);
}


// === tick/date formatting functions ===
// from: https://stackoverflow.com/questions/20010864/d3-axis-labels-become-too-fine-grained-when-zoomed-in

export function timeFormat(formats) {
  return function(date) {
    var i = formats.length - 1, f = formats[i];
    while (!f[1](date)) f = formats[--i];
    return f[0](date);
  };
};

function customTickFunction(t0, t1, dt)  {
    var labelSize = 42; //
    var maxTotalLabels = Math.floor(this.width / labelSize);

    function step(date, offset)
    {
        date.setMonth(date.getMonth() + offset);
    }

    var time = d3.time.month.ceil(t0), times = [], monthFactors = [1,3,4,12];

    while (time < t1) times.push(new Date(+time)), step(time, 1);
    var timesCopy = times;
    var i;
    for(i=0 ; times.length > maxTotalLabels ; i++)
        times = _.filter(timesCopy, function(d){
            return (d.getMonth()) % monthFactors[i] == 0;
        });

    return times;
};


// === brush and zoom functions ===

function brushed() {

    this.x.domain(this.brush.empty() ? this.x2.domain() : this.brush.extent());
    this.focus.select(".area").attr("d", this.area);
    this.focus.select(".line").attr("d", this.line);
    this.focus.select(".x.axis").call(this.xAxis);
    // Reset zoom scale's domain
    this.zoom.x(this.x);
    updateDisplayDates();
    setYdomain();

};

function draw() {
    setYdomain();
    this.focus.select(".area").attr("d", this.area);
    this.focus.select(".line").attr("d", this.line);
    this.focus.select(".x.axis").call(this.xAxis);
    //focus.select(".y.axis").call(yAxis);
    // Force changing brush range
    this.brush.extent(x.domain());
    this.component.select(".brush").call(this.brush);
    // and update the text showing range of dates.
    updateDisplayDates();
};

function brushend() {
// when brush stops moving:

    // check whether chart was scrolled out of bounds and fix,
    var b = this.brush.extent();
    var out_of_bounds = this.brush.extent().some(function(e) { return e < this.date_limits.inicio | e > this.date_limits.final; });
    if (out_of_bounds){ b = moveInBounds(b); };

};

function updateDisplayDates() {

    var b = this.brush.extent();
    // update the text that shows the range of displayed dates
    var localBrushDateStart = (brush.empty()) ? DateFormat(dataXrange[0]) : DateFormat(b[0]),
        localBrushDateEnd   = (brush.empty()) ? DateFormat(dataXrange[1]) : DateFormat(b[1]);

    // Update start and end dates in upper right-hand corner
    d3.select("#displayDates")
        .text(localBrushDateStart == localBrushDateEnd ? localBrushDateStart : localBrushDateStart + " - " + localBrushDateEnd);
};

function moveInBounds(b) {
// move back to boundaries if user pans outside min and max date.

    var ms_in_year = 31536000000,
        brush_start_new,
        brush_end_new;

    if       (b[0] < mindate)   { brush_start_new = mindate; }
    else if  (b[0] > maxdate)   { brush_start_new = new Date(maxdate.getTime() - ms_in_year); }
    else                        { brush_start_new = b[0]; };

    if       (b[1] > maxdate)   { brush_end_new = maxdate; }
    else if  (b[1] < mindate)   { brush_end_new = new Date(mindate.getTime() + ms_in_year); }
    else                        { brush_end_new = b[1]; };

    brush.extent([brush_start_new, brush_end_new]);

    brush(d3.select(".brush").transition());
    brushed();
    draw();

    return(brush.extent())
};

function setYdomain(){
// this function dynamically changes the y-axis to fit the data in focus

    // get the min and max date in focus
    var xleft = new Date(x.domain()[0]);
    var xright = new Date(x.domain()[1]);

    // a function that finds the nearest point to the right of a point
    var bisectDate = d3.bisector(function(d) { return d.month; }).right;

    // get the y value of the line at the left edge of view port:
    var iL = bisectDate(dataset, xleft);

    if (dataset[iL] !== undefined && dataset[iL-1] !== undefined) {

        var left_dateBefore = dataset[iL-1].month,
            left_dateAfter = dataset[iL].month;

        var intfun = d3.interpolateNumber(dataset[iL-1].count, dataset[iL].count);
        var yleft = intfun((xleft-left_dateBefore)/(left_dateAfter-left_dateBefore));
    } else {
        var yleft = 0;
    }

    // get the x value of the line at the right edge of view port:
    var iR = bisectDate(dataset, xright);

    if (dataset[iR] !== undefined && dataset[iR-1] !== undefined) {

        var right_dateBefore = dataset[iR-1].month,
            right_dateAfter = dataset[iR].month;

        var intfun = d3.interpolateNumber(dataset[iR-1].count, dataset[iR].count);
        var yright = intfun((xright-right_dateBefore)/(right_dateAfter-right_dateBefore));
    } else {
        var yright = 0;
    }

    // get the y values of all the actual data points that are in view
    var dataSubset = dataset.filter(function(d){ return d.month >= xleft && d.month <= xright; });
    var countSubset = [];
    dataSubset.map(function(d) {countSubset.push(d.count);});

    // add the edge values of the line to the array of counts in view, get the max y;
    countSubset.push(yleft);
    countSubset.push(yright);
    var ymax_new = d3.max(countSubset);

    if(ymax_new == 0){
        ymax_new = dataYrange[1];
    }

    // reset and redraw the yaxis
    y.domain([0, ymax_new*1.05]);
    focus.select(".y.axis").call(yAxis);

};

function scaleDate(d,i) {
// action for buttons that scale focus to certain time interval

    var b = brush.extent(),
        interval_ms,
        brush_end_new,
        brush_start_new;

    if      (d == "year")   { interval_ms = 31536000000}
    else if (d == "month")  { interval_ms = 2592000000 };

    if ( d == "year" | d == "month" )  {

        if((maxdate.getTime() - b[1].getTime()) < interval_ms){
        // if brush is too far to the right that increasing the right-hand brush boundary would make the chart go out of bounds....
            brush_start_new = new Date(maxdate.getTime() - interval_ms); // ...then decrease the left-hand brush boundary...
            brush_end_new = maxdate; //...and set the right-hand brush boundary to the maxiumum limit.
        } else {
        // otherwise, increase the right-hand brush boundary.
            brush_start_new = b[0];
            brush_end_new = new Date(b[0].getTime() + interval_ms);
        };

    } else if ( d == "data")  {
        brush_start_new = dataXrange[0];
        brush_end_new = dataXrange[1]
    } else {
        brush_start_new = b[0];
        brush_end_new = b[1];
    };

    brush.extent([brush_start_new, brush_end_new]);

    // now draw the brush to match our extent
    brush(d3.select(".brush").transition());
    // now fire the brushstart, brushmove, and brushend events
    brush.event(d3.select(".brush").transition());
};
