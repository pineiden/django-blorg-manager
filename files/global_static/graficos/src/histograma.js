import * as d3 from "d3";

/*
TODO:

responsive: http://www.cagrimmett.com/til/2016/04/26/responsive-d3-bar-chart.html
tooltips : 

 */

export default function histograma(selector, opts){
    let chart = {};
    let component = d3.select("#"+selector)
        .append("svg")
        .attr("class", "histogram-"+selector)
        .attr("width", 600)
        .attr("height", 300);
    const margin = {
      top: 20,
      right: 20,
      bottom: 30,
      left: 100
    };
    const width = +component.attr("width") - margin.left - margin.right;
    const height = +component.attr("height") - margin.top - margin.bottom;
    /*
      Se crea un nuevo grupo que permita dibujar y dejar un espacio para las ticks

     */
    let graph = component.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    /*
      create scale
     */
    var parseTime = d3.timeParse("%d-%b-%y");


    var x = d3.scaleBand().domain(opts.histograma.bins).range([0,width]);
    x.paddingInner(0.05);

    var y = d3.scaleLinear()
        .rangeRound([height, 0]);

    /*

     */
     function base_medida(valor, sep){
        /*
          eJ:
          10 % --> [10, %]

        */
        let array = valor.label.split(sep);
        return array;
     };

    function tooltips(){
        /*
          https://bl.ocks.org/bytesbysophie/0311395c1e082f98e67efaf2c7f9555b
         */
        var tip = d3.tip().attr('class', 'd3-tip').direction('e').offset([0,5])
            .html(function(d) {
                var content = "<span style='margin-left: 2.5px;'><b>" + d.key + "</b></span><br>";
                content +=`
                    <table style="margin-top: 2.5px;">
                            <tr>
                                <td>Valor: </td>
                                <td style="text-align: right">` + d3.format(".2f")(d.value) + `</td>
                             </tr>
                            <tr>
                                <td>Porcentual: </td>
                                <td style="text-align: right">` + d3.format(".2f")(d["porcentual"]) + `</td>
                             </tr>
                    </table>
                    `;
                return content;
            });
        component.call(tip);

        return tip;

    }

    function gettime(seconds){
        const format = d3.format(".2f");
        const hour = 60*60;
        const day = 60*60*24;
        if (seconds>60 && seconds<hour){
            return format(seconds/60) +"[min]";
        }
        else if (seconds>=hour && seconds<day){
            return format(seconds/hour) + "[hr]";
        }
        else if (seconds>=day){
            return format(seconds/day) + "[dias]";
        }
        else
        {
            return seconds +"[s]";
        }


    }

    chart.init = function init(data_input){
        // format the data
        let data = data_input.values;
        let title = data_input.key;

        data.forEach(function(d) {
            let u= base_medida(d," ");
            d.base = parseFloat(u[0]);
            d.medida = u[1];
          });


          // Scale the range of the data in the y domain
        y.domain([0, d3.max(data, function(d) { return d.value; })]);

        let yAxis=d3.axisLeft(y);
        let xAxis=d3.axisBottom(x);
        // Define the div for the tooltip
        var div = d3.select("body").append("div")	
            .attr("class", "tooltip")				
            .style("opacity", 0);

        // add the x Axis
        graph.append("g")
            .attr("class","x-axis")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis);

        // add the y Axis
        graph.append("g")
            .attr("class","y-axis")
            .call(yAxis);

          // append the bar rectangles to the svg element
        graph.selectAll("rect")
              .data(data)
              .enter().append("rect")
              .attr("class", "bar")
              .attr("x", 1)
              .attr("transform", function(d) {
                  return "translate(" + x(d.base) + "," + y(d.value) + ")"; })
              .attr("width",  x.bandwidth())
            .attr("height", function(d) { return height - y(d.value); })
            .on('mouseover',function(d) {
            div.transition()		
                .duration(200)		
                .style("opacity", .9);		
                div	.html(`<section>`+gettime(d.value)+`</section>`)	
                .style("left", (d3.event.pageX) + "px")		
                .style("top", (d3.event.pageY - 28) + "px");	
            } )
            .on('mouseout', function(d) {		
                div.transition()		
                    .duration(500)		
                    .style("opacity", 0);	
            });


        /*
          Put tooltips

        */
    };

    return chart;
}

export {histograma};
