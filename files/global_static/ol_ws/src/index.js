/*
rxjs tools
 */
import { interval } from 'rxjs';
import { mapTo } from 'rxjs/operators';

/*
  from @csn_chile/status_map_gnss
*/
import 'ol/ol.css';

let smg = require('@csn_chile/status_map_gnss');

//let smg = require('./olmap');


/*
  from @csn_chile/wsjs_charts
*/

let ts = require('@csn_chile/table_status');
// let ts = require('./table_status');


/*

Activate websocket

 */

import {w3c_websocket_client, getDataWS, buildURI } from './ws_client';

/*
Create new function that moves to this point of the map
*/
/*
Coordinate Transform
 */
import { fromLonLat } from 'ol/proj';


window.addEventListener('DOMContentLoaded', function () {
    const completeTable = ts.complete_table;
    const getStations = ts.getStations;
    const getOpts = ts.getOpts;
    const tableMap = ts.table_map;
    const updateTable = ts.update_table;
    const updateField = ts.updateField;
    const columnMap= ts.column_map;
    const CODIGO = 'Código';
    const NOMBRE = 'Nombre';
    const BATERIA = 'Batería';
    const MEMORIA = 'Memoria';
    const ULTIMO_DATO = 'Último dato';
    const TPO_OFFLINE = 'Tiempo Offline';
    const DOP = 'Dilution of Precision';
     
    let otherOpts = getOpts();

    function LonLat(data){
        return [data.pos.lon, data.pos.lat];
    }

    function flyTo(view, location, done) {
        var duration = 3000;
        var zoom = 8;
        var parts = 2;
        var called = false;
        function callback(complete) {
            --parts;
            if (called) {
                return;
            }
            if (parts === 0 || !complete) {
                called = true;
                done(complete);
            }
        }
        view.animate({
            center: location,
            duration: duration
        }, callback);
        view.animate({
            zoom: zoom - 4,
            duration: duration / 2
        }, {
            zoom: zoom,
            duration: duration / 2
        }, callback);
    }

    function connectTableRowMapPoint(data, table_row, map,){
        const lonlat = LonLat(data);
        const location = fromLonLat(lonlat);
        let view = map.getView();
        table_row.addEventListener('click', function moveTo(){
            flyTo(view, location, function(){});
        });

    };


    //const stationsList = getStations();
    const stationsList = getStations();
    let init_zoom = 6;
    let map = smg.defaultInit("map", stationsList, init_zoom);
          otherOpts['row_click']={
              callback:connectTableRowMapPoint,
              args:[map]};

    const uri=getDataWS();


    

	let key_name_json = {
	  [CODIGO]: ['station'],
	  [NOMBRE]: ['name'],
	  [BATERIA]: ['batt_cap'],
	  [MEMORIA]: ['remaining_mem'],
	  [ULTIMO_DATO]: ['timestamp'],
	  [TPO_OFFLINE]: ['time_connected'],
	  [DOP]: ['dop'],
	};

        let periodic_task = function(){
            ts.update_field("data_table", ULTIMO_DATO, ts.deltatime, [], tableMap, columnMap);
        };


 w3c_websocket_client(uri,
                      completeTable,
                      updateTable,
                      key_name_json,
                      stationsList,
                      tableMap,
                      otherOpts,
                      columnMap,
                      periodic_task);



});
