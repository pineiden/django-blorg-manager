let WebSocketServer = require('websocket').server;
let WebSocketClient = require('websocket').client;
let WebSocketFrame = require('websocket').frame;
let WebSocketRouter = require('websocket').router;
let W3CWebSocket = require('websocket').w3cwebsocket;


import {webSocket} from 'rxjs/webSocket';
import {map,filter, pluck} from 'rxjs/operators';

export function buildURI(ws_data) {
    if (ws_data.scheme==="wss"){
        const uri = ws_data.scheme+'://' + ws_data.ip + '/' + ws_data.path;
        return uri;}
    else{
        const uri = ws_data.scheme+'://' + ws_data.ip + ':' + ws_data.port + '/' + ws_data.path;
        return uri;
    }
}

export function getDataWS(){
    const wsData = document.getElementById('websocket_data_json').textContent;
    const wsDataJSON = JSON.parse(wsData);
    const uri = buildURI(wsDataJSON);
    return uri;
}


export function w3c_websocket_client(uri,
                                     completeTable,
                                     updateTable,
                                     keyNameJSON,
                                     stationList,
                                     tableMap,
                                     otherOpts,
                                     columnMap,
                                     periodic_task) {
    let client = webSocket({
        url:uri,
        openObserver: {
            next: ()=>{
                console.log(`Websocket on ${uri} opened`);
                completeTable('data_table', keyNameJSON, stationList, tableMap, otherOpts);}
        },
        closeObserver :{
            next: ()=>{
                console.log(`Websocket on ${uri} closed`);
                console.log("Closing websocket");}
        }

    });

    client.pipe(
        filter(data=>document.visibilityState==='visible')
    ).subscribe({
        next: data_json =>{
            updateTable(data_json, 'data_table', keyNameJSON, tableMap, columnMap, otherOpts);
            periodic_task();
            data_json = null;
        },
        error: (e) => console.log('Connection Error',e),
        complete : () => console.log("Complete Ended websocket")}
    );

}
