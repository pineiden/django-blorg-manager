# -*- coding: utf-8 -*-

import decimal
from django.conf import settings


class DecimalPrecisionMiddleware(object):
    def process_request(self, request):
        decimal_context = decimal.getcontext()
        decimal_context.prec = 4 # say: 4
