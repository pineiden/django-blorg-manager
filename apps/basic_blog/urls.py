from django.urls import path, re_path, include
from .views import RepositoryIndexView, RepositoryBlogView
from django.conf import settings

BLOGS_REGEX = settings.BLOGS_REGEX

urlpatterns = [
    path('', RepositoryIndexView.as_view()),
    path('id:<int:repo_id>', RepositoryBlogView.as_view()),
    path('<str:repo_name>', RepositoryBlogView.as_view()), #UUID repo
    re_path(r'<str:repo_name>/(?P<path>{regex})'.format(regex=BLOGS_REGEX), 
            RepositoryBlogView.as_view()),
]
