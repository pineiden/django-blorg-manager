from django.shortcuts import render
from django.views.generic import TemplateView, ListView, DetailView
from apps.repository.models import Repository, RepositoryLog
from datetime import datetime
from rich import print
from django.utils.text import slugify
# Create your views here.
# modo prueba generar el blorgtree:
from django.core.cache import cache
from blorgtree import BlorgTree
from pathlib import Path
from django.utils.safestring import SafeString

"""
The path to some file must be expressed with .:
blog.index.html
blog.abril.index.html
blog.MAYO.index.html

Debe tomar el árbol dual y extraer el archivo

tb: puede ser por un indentificador único UUID asociado a la ruta.
"""

class RepositoryIndexView(ListView):
    template_name = 'basic_blog/list.dj.html'
    model = Repository

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['utc'] = datetime.utcnow()
        return context


class RepositoryBlogView(TemplateView):
    template_name = 'basic_blog/view.dj.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        buscar_id = "repo_id" in kwargs 
        buscar_name = "repo_name" in kwargs 
        repo = None
        if buscar_id:
            repo = Repository.objects.filter(id=kwargs["repo_id"]).first()
            print(context["repository"].to_dict())
        if buscar_name:
            slug = slugify(kwargs["repo_name"])
            repo = Repository.objects.filter(slug=slug).first()
        if repo:
            btree = cache.get(repo.slug)
            if not btree:
                repo_log = repo.log.filter(action=RepositoryLog.Action.CLONE).first()
                repo_path = Path(repo_log.result.get("path")) / "config.yml"
                btree = BlorgTree(repo.slug, repo_path)
                cache.set(repo.slug, btree)
            context["blorg"] = SafeString(btree.htmls.toJSON())
            context["repository"] = repo        
        context['utc'] = datetime.utcnow()
        return context


class RepositoryBlogViewID(RepositoryBlogView):
    template_name = 'basic_blog/view.dj.html'
