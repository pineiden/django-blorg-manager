from django.apps import AppConfig


class BasicBlogConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.basic_blog'
