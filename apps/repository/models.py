from django_extensions.db.fields import AutoSlugField
from django.db import models
from functools import partial
from pathlib import Path
from apps.base.models import BaseModel
from django.utils.translation import gettext_lazy as _
from django.conf import settings
from git import Repo
from rich import print


class Repository(BaseModel):
    name = models.CharField(
        max_length=256)
    slug = AutoSlugField(
        populate_from='name')
    url = models.URLField(
        max_length=2600)
    branch = models.CharField(
        max_length=32, 
        default="public")
    private = models.BooleanField(
        default=True)
    token = models.CharField(
        max_length=1024)

    def __repr__(self):
        return f"Repository({self.name}, {self.url}, {self.branch})"

    def __str__(self):
        return f"Repository({self.name}, {self.url}, {self.branch})"

    class Meta:
        app_label = "repository"
        verbose_name = _("Repository")
        verbose_name_plural = _("Repositories")
        ordering = ("created", "name", "branch")

    @property
    def git_url(self):
        if self.token and self.private:
            token_access = f"https://gitlab-ci-token:{self.token}@"
            return self.url.replace("https://", token_access)
        elif not self.token and self.private:
            token_access = f"https://:@"
            return self.url.replace("https://", token_access)
        else:
            return self.url

    def to_dict(self):
        result = super().to_dict()
        commited_date = self.last_commit.committed_date  if self.last_commit else None
        result.update({
            "path": str(self.path),
            "name": self.name,
            "url": self.git_url,
            "branch": self.branch,
            "last_commit": str(self.last_commit.message),
            "commited_date":commited_date,
            "slug":self.slug
        })
        return result

    @property
    def path(self):
        path = settings.BLOGS_DIR / self.slug
        return path

    @property
    def exists(self):
        return self.path.exists()

    @property
    def repo_path(self):
        if self.exists:
            return Repo(self.path)

    @property
    def origin(self):
        return self.repo_path.remotes.origin

    @property
    def git(self):
        if self.exists:
            return self.repo_path.git
        
    @property
    def remote(self):
        if self.exists:
            return self.repo_path.remote()

    @property
    def last_commit(self):
        if self.exists:
            return self.repo_path.head.commit

    @property
    def active_branch(self):
        if self.exists:
            return self.repo_path.active_branch.name
        return ""
    
    @property
    def branches(self):
        if self.exists:
            return [b.name.lower() for b in self.repo_path.branches]
        return []

    @property
    def remote_branches(self):
        if self.exists:
            name = lambda b: b.name.lower()
            return {name(b):b for b in self.remote}
        return []


class MoveToBranch(BaseModel):
    repository = models.ForeignKey(
        Repository, 
        on_delete=models.CASCADE)
    branch = models.CharField(
        max_length=32, 
        default="public")

    class Meta:
        app_label = "repository"
        verbose_name = _("MoveToBranch")
        verbose_name_plural = _("MoveToBranches")
        ordering = ("created", "repository", "branch")

    def __repr__(self):
        return f"MoveToBranch({self.repository.name}, {self.repository.branch} -> {self.branch})"

    def __str__(self):
        return f"MoveToBranch({self.repository.name}, {self.repository.branch} -> {self.branch})"

    @property
    def active_branch(self):
        return self.repository.active_branch.lower()

    @property
    def exists(self):
        return self.branch.lower() in self.repository.branches

    def in_remote(self):
        branch = self.branch.lower()
        for r, v in self.repository.remote_branches.items():
            if r.endswith(f"/{branch}"):
                return True, v
        return False, None

    def move(self):
        """
        Move to branch, if not here fetch from remote, else False
        """
        branch = self.branch.lower()
        in_remote, remote = self.in_remote()
        if self.repository.exists and self.exists:
            self.repository.git.checkout(branch)
            return True
        elif self.repository.exists and in_remote:
            remote.fetch()
            self.repository.git.checkout(branch)
            return True
        return False


class RepositoryLog(BaseModel):
    class Action(models.TextChoices):
        CLONE = "CL", _("Clone")
        UPDATE = "UP", _("Update")
        BRANCH = "BR", _("Branch")
        DELETE = "DE", _("Delete")
        FAIL = "FA", _("Fail")

    repository = models.ForeignKey(
        Repository, 
        on_delete=models.CASCADE, 
        null=True, 
        blank=True, 
        related_name='log')
    action = models.CharField(
        max_length=2, 
        choices=Action.choices, 
        default=Action.UPDATE)
    success = models.BooleanField(
        default=False)
    result = models.JSONField()
    message = models.CharField(max_length=2048)

    def __str__(self):
        return f"RepositoryLog({self.repository.name}, {self.action}, {self.success}, {self.message})"

    class Meta:
        app_label = "repository"
        verbose_name = _("RepositoryLog")
        verbose_name_plural = _("RepositoriesLogs")
        ordering = ("created", "repository", "success")

    def to_dict(self):
        result = super().to_dict()
        commited_date = None
        repo = None
        if self.repository:
            commited_date = self.repository.last_commit.committed_date 
            repo = self.repository
        result.update({
            "repository": repo.to_dict() if repo else None,
            "action": self.action,
            "success": self.success,
            "result": self.result,
            "message": self.message,
            "commited_date": commited_date
        })
        return result
