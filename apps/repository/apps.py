from django.apps import AppConfig


class RepositoryConfig(AppConfig):
    name = 'apps.repository'

    def ready(self):
        import apps.repository.signals
