from celery import shared_task
from django.core.cache import cache
from django.conf import settings
from .models import (Repository, RepositoryLog, MoveToBranch)
import time
import shutil
from datetime import datetime
from git import Repo
from typing import Dict, Any
from rich import print
from pathlib import Path
from blorgtree import BlorgTree
from pathlib import Path
from django.utils.safestring import SafeString

@shared_task
def git_clone(instance_id:int):
    try:
        instance = Repository.objects.get(id=instance_id)
        creator = instance.creator
        timeout = settings.GIT_TIMEOUT
        if instance.private and instance.token:
            def clone_repo():
                repo = Repo.clone_from(
                    instance.git_url, 
                    instance.path, 
                    depth=settings.GIT_CLONE_DEPTH,
                    kill_after_timeout=timeout)
                return repo
            repo = clone_repo()
            last_commit = repo.head.commit
            cdate = last_commit.committed_date
            result = {
                "creator": creator,
                "repository": instance,
                "success": True, 
                "action": RepositoryLog.Action.CLONE,
                "result": {
                    "cloned_at": datetime.utcnow().isoformat(),
                    "last_commit": last_commit.message, 
                    "last_commit_date": cdate,
                    "path": str(instance.path)
                },
                "message": "Repository cloned successfully"
            }
            rep_log = RepositoryLog.objects.create(**result)
            # move branch
            if instance.branch != instance.active_branch:
                MoveToBranch.objects.create(
                    creator=creator, 
                    repository=instance, 
                    branch=instance.branch)
            return rep_log.to_dict()
        else:
            result = {
                "creator": creator,
                "action": RepositoryLog.Action.FAIL,
                "repository": instance,
                "success": False, 
                "result": {},
                "message": "Repository is private, needs a token"
            }
            rep_log = RepositoryLog.objects.create(**result)
            return rep_log.to_dict()
    except Exception as e:
        return {"success": False, "error":str(e)}



@shared_task
def git_change_branch(instance_id:int):
    try:
        instance = MoveToBranch.objects.get(id=instance_id)
        creator = instance.creator

        if instance.exists():
            ok = instance.move()
            last_commit = instance.repository.last_commit
            cdate = last_commit.committed_date
            result = {
                "creator": creator,
                "repository": instance.repository,
                "success": ok, 
                "action": RepositoryLog.Action.BRANCH,
                "result": {
                    "last_commit": last_commit.message, 
                    "last_commit_date": cdate,
                    "path": str(instance.repository.path)
                },
                "message": f"Repository moved to {instance.branch}"
            }
            rep_log = RepositoryLog.objects.create(**result)
            return rep_log.to_dict()
        else:
            result = {
                "creator": creator,
                "repository": instance.repository,
                "success": False, 
                "action": RepositoryLog.Action.FAIL,
                "result": {
                },
                "message": f"Repository can't be moved to {instance.branch}"
            }
            rep_log = RepositoryLog.objects.create(**result)
            return rep_log.to_dict()
    except Exception as e:
        return {"success": False, "error":str(e)}


def update_btree(repo:Repository):
    repo_log = repo.log.filter(action=RepositoryLog.Action.CLONE).first()
    repo_path = Path(repo_log.result.get("path")) / "config.yml"
    btree = BlorgTree(repo.slug, repo_path)
    cache.set(repo.slug, btree)

@shared_task
def git_update(instance_id:int):
    try:
        instance = Repository.objects.get(id=instance_id)
        creator = instance.creator
        update_btree(instance)
        if instance.exists:
            """
            Git fetch all remote branches
            Git pull from actual branch
            """
            before_commit = instance.last_commit
            instance.origin.pull()
            last_commit = instance.last_commit
            if before_commit != last_commit:
                cdate = last_commit.committed_date
                result = {
                    "creator": creator,
                    "repository": instance,
                    "success": True, 
                    "action": RepositoryLog.Action.UPDATE,
                    "result": {
                        "last_commit": last_commit.message, 
                        "last_commit_date": cdate,
                        "path": instance.repository.path
                    },
                    "message": f"Repository updated at {instance.active_branch}"
                }
                rep_log = RepositoryLog.objects.create(**result)
                return rep_log.to_dict()
            else:
                return {
                    "message": f"Repository not updated","success": True}
        else:
            return {
                "creator": creator,
                "repository": instance,
                "success": False,
                "result": {},
                "message": f"Repository doesn't exists"
            }
    except Exception as e:
        return {"success": False, "error":str(e)}



@shared_task
def git_delete(data:Dict[str, Any]):
    try:
        BD = settings.BLOGS_DIR.resolve()
        name = data["name"]
        url = data["url"]
        path = Path(data["path"])
        repo = Repo(path)
        before_commit = data["last_commit"]
        commited_date = data["commited_date"]
        # delete instance and check if inside
        if BD in path.resolve().parents:
            print(f"Deleting path {path}")
            shutil.rmtree(path)
        result = {
            "repository": None,
            "success": True, 
            "action": RepositoryLog.Action.DELETE,
            "result": {
                "url":url,
                "name": name,
                "deleted_at": datetime.utcnow().isoformat(),
                "last_commit": before_commit, 
                "last_commit_date": commited_date,
                "path": str(path)
            },
            "message": f"Repository deleted for {name}/{url}"
        }
        rep_log = RepositoryLog.objects.create(**result)
        result = rep_log.to_dict()
        return result
    except Exception as e:
        return {"success": False, "error":str(e)}
