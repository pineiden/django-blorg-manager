# Generated by Django 4.0.5 on 2022-06-13 01:30

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('repository', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='repository',
            options={'ordering': ('created', 'name', 'branch'), 'verbose_name': 'Repository', 'verbose_name_plural': 'Repositories'},
        ),
        migrations.AddField(
            model_name='repository',
            name='branch',
            field=models.CharField(default='public', max_length=32),
        ),
        migrations.AddField(
            model_name='repository',
            name='private',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='repository',
            name='slug',
            field=django_extensions.db.fields.AutoSlugField(blank=True, editable=False, populate_from='name'),
        ),
        migrations.AddField(
            model_name='repository',
            name='token',
            field=models.CharField(default='', max_length=1024),
            preserve_default=False,
        ),
        migrations.CreateModel(
            name='RepositoryLog',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('action', models.CharField(choices=[('CL', 'Clone'), ('UP', 'Update'), ('BR', 'Branch'), ('DE', 'Delete'), ('FA', 'Fail')], default='UP', max_length=2)),
                ('success', models.BooleanField(default=False)),
                ('result', models.JSONField()),
                ('message', models.CharField(max_length=2048)),
                ('creator', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL)),
                ('repository', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='repository.repository')),
            ],
            options={
                'verbose_name': 'Repository',
                'verbose_name_plural': 'Repositories',
                'ordering': ('created', 'repository', 'success'),
            },
        ),
        migrations.CreateModel(
            name='MoveToBranch',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('branch', models.CharField(default='public', max_length=32)),
                ('creator', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL)),
                ('repository', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='repository.repository')),
            ],
            options={
                'verbose_name': 'Repository',
                'verbose_name_plural': 'Repositories',
                'ordering': ('created', 'repository', 'branch'),
            },
        ),
    ]
