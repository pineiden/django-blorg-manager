from rest_framework import serializers
from django_restql.mixins import DynamicFieldsMixin
from ..models import (Repository,RepositoryLog, MoveToBranch)


class RepositorySerializer(
        DynamicFieldsMixin,
        serializers.Serializer):

    class Meta:
        model = Repository
        fields = [
            "id","creator","name","url"
        ]


class RepositoryLogSerializer(
        DynamicFieldsMixin,
        serializers.Serializer):

    class Meta:
        model = RepositoryLog
        fields = [
            "id","creator","name","url", 
            "repository", "success","result","message", 
            "action"
        ]


class MoveToBranchSerializer(
        DynamicFieldsMixin,
        serializers.Serializer):

    class Meta:
        model = RepositoryLog
        fields = [
            "id","creator","name", "repository", 
            "branch"
        ]
