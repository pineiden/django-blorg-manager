from .viewsets import (RepositoryViewset)


repository_route_list = [
    (r"repository", RepositoryViewset)
]
