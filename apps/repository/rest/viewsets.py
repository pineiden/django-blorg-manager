from rest_framework import viewsets
from .serializers import (RepositorySerializer)
from ..models import (Repository,RepositoryLog, MoveToBranch)


class RepositoryViewset(viewsets.ModelViewSet):
    queryset = Repository.objects.all()
    serializer_class = RepositorySerializer

    
