from django.contrib.gis import admin
from .models import (Repository, MoveToBranch, RepositoryLog)


# Register your models here.
@admin.register(Repository)
class RepositoryAdmin(admin.ModelAdmin):
    date_hierarchy = "created"
    list_display = ('created', 'name', 'url', "branch", "private")


@admin.register(MoveToBranch)
class MoveToBranchAdmin(admin.ModelAdmin):
    date_hierarchy = "created"
    list_display = ('created', 'repository', 'branch')


@admin.register(RepositoryLog)
class RepositoryLogAdmin(admin.ModelAdmin):
    date_hierarchy = "created"
    list_display = ('created', 'repository', 'action', "success")
