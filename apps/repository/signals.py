from django.db.models.signals import post_save, pre_save, pre_delete
from django_celery_beat.models import (PeriodicTask, IntervalSchedule,
                                       CrontabSchedule)
from django.dispatch import receiver
from .tasks import (git_clone, git_change_branch, git_update, git_delete)
from .models import (Repository, RepositoryLog, MoveToBranch)
from django.conf import settings
from rich import print

"""
On create a repository
"""

@receiver(post_save, sender=Repository)
def create_repository(sender, instance, created, **kwargs):
    if created:
        task = git_clone.delay(instance.id)
        crontab = settings.GIT_UPDATE_CRONTAB
        if crontab:
            schedule, created = CrontabSchedule.objects.get_or_create(
                **crontab)

            
            periodic_task, created = PeriodicTask.objects.get_or_create(
                crontab=schedule,
                task="apps.repository.tasks.git_update",
                name=f"Repository({instance.name}) Git Update Periodic Task",
            )
            periodic_task.args =  [instance.id]
            periodic_task.save()


@receiver(pre_delete, sender=Repository, dispatch_uid='delete_repository')
def delete_repository(sender, instance, using, **kwargs):
    data = instance.to_dict()
    task = git_delete.delay(data)


@receiver(post_save, sender=MoveToBranch)
def move_to_branch(sender, instance, created, **kwargs):
    if created:
        task = git_change_branch.delay(instance.id)
