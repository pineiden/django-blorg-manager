from django.apps import AppConfig


class DualTreeConfig(AppConfig):
    name = 'apps.dual_tree'
