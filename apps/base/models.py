from django.db import models
from django.contrib.auth.models import User
from datetime import datetime
from rich import print
from django.db.models.fields.files import FileField


class BaseModel(models.Model):
    """
    This model class has common fields to use on the other
    classes
    """
    created = models.DateTimeField(auto_now_add=True, blank=True)
    modified = models.DateTimeField(auto_now=True, blank=True)
    creator = models.ForeignKey(
        User,
        on_delete=models.PROTECT)

    class Meta:
        abstract = True
        ordering = ["-created"]

    def to_dict(self) -> object:
        if self.id:
            return {
                "id": self.id,
                "model": self.__class__.__name__.lower(),
                "created": self.created.isoformat(),
                "modified": self.modified.isoformat(),
                "creator": {
                    "id": self.creator.id,
                    "username": self.creator.username
                }
            }
        else:
            return {}

    def file_fields(self, exclude=[]):
        """
        If exclude is not empty, discard field with names
        in exclude
        """
        fields = self._meta.get_fields()

        def name(f): return str(f).split(".")[-1]
        filefields = list(
            filter(
                lambda e: isinstance(e, FileField) and name(e) not in exclude,
                fields)
        )

        return filefields
