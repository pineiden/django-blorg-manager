from django.test import TestCase
from openpyxl import load_workbook
from django.conf import settings
from topoapi.functions.displacements_calculator import DisplacementsCalculator



class CuadrantesTest(TestCase):
    
pth = "/".join()str()settings.BASE_DIR), 'topoapi/excel-examples'
wb = load_workbook('./')


disp = DisplacementsCalculator()

data_sequence = []
with open('/home/francisco/Quux/Proyecto_geosinergia_metro/app/excel-examples/datos.txt', 'r') as reader:
    line = reader.readline()
    while line:
        aux = line.rstrip().split()
        if len(aux) == 0:
            break
        data = dict()
        data['status'], data['north'], data['east'], data['elevation'] = (float(x) for x in aux)
        data_sequence.append(data)
        line = reader.readline()

disp.set_azimuth(28.0)
displ_sequence = disp.compute_CLU_displacements_sequence(data_sequence)

for x, y, z in displ_sequence:
    print(f'{x}  {y}  {z}')
