from django.conf import settings
from datetime import timedelta
from functools import partialmethod
if settings.IS_MODULE:
    from topoapi.apps.file_manager.models import (TopoFileReview,
                                                  TopoFileReport)
    from topoapi.apps.spreadsheet_review.models import SpreadsheetReview
else:
    from apps.file_manager.models import (TopoFileReview,
                                          TopoFileReport)
    from apps.spreadsheet_review.models import SpreadsheetReview
import djclick as click
import csv


# TODO  rehacer
class CompareModelObjects(object):

    FLOAT_TOLERANCE = 1.e-6
    TIME_DELTA_TOLERANCE = timedelta(milliseconds=999)

    MODELS = {

        # file_manager ----
        "TopoFileReview": {
          "class": TopoFileReview,
          "fields": (("topofile", "TopoFile"),
                     ("status", "str"),
                     ("confirmed", "bool"))
        },
        "TopoFileReport": {
          "class": TopoFileReport,
          "fields": (("topofile_review", "TopoFileReview"),
                     ("report", "file"))
        },
        # spreadsheet_review ----
        "SpreadSheetReview": {
          "class": SpreadsheetReview,
          "fields": (("topofile_review", "TopoFileReview"),
                     ("spreadsheet", "file"),
                     ("validation_report", "file"),
                     ("validation_success", "bool"))
        }
    }

    MODELS_FIELDS = {model: tuple(field_tuple[0] for field_tuple in model_dict["fields"])
                     for model, model_dict in MODELS.items()}

    MODELS_KEYS = set(MODELS)

    @staticmethod
    def compare_simple(field_db, field_csv):
        return field_db == field_csv

    compare_str = compare_simple

    compare_int = compare_simple

    compare_bool = compare_simple

    @staticmethod
    def compare_float(float_db, float_csv):
        return abs(float_db-float_csv) < CompareModelObjects.FLOAT_TOLERANCE

    @staticmethod
    def compare_datetime(datetime_db, datetime_csv):
        return (-CompareModelObjects.TIME_DELTA_TOLERANCE
                < (datetime_db - datetime_csv)
                < CompareModelObjects.TIME_DELTA_TOLERANCE)

    @staticmethod
    def compare_email(object_db, username_csv, email_csv):
        email = email_csv
        if email_csv.startswith("@"):
            email = f"{username_csv}{email}"
        return getattr(object_db, "email") == email

    @staticmethod
    def compare_file(field, object_db, filename_csv):
        pass

    @staticmethod
    def compare_any_field(field, object_db, value_csv):
        return getattr(CompareModelObjects,
                       f"compare_{field}")(object_db, value_csv)

    @staticmethod
    def compare_model(model, object_db, values_csv):
        fields_model = CompareModelObjects.MODELS[model]["fields"]
        return all(
            CompareModelObjects.compare_any_field(field[0], object_db, value)
            for field, value in zip(fields_model, values_csv))

    @staticmethod
    def compare_simple_model_field(field, object_db, value_csv):
        return getattr(object_db, field) == value_csv

    compare_user = partialmethod(compare_simple_model_field, "username")

    compare_Axis = partialmethod(compare_model, "Axis")

    compare_ExcavationPhase = partialmethod(compare_model, "ExcavationPhase")

    compare_TopoFile = partialmethod(compare_model, "TopoFile")

    compare_TopoFileReview = partialmethod(compare_model, "TopoFileReview")

    compare_Note = partialmethod(compare_model, "Note")

    compare_ReportCreated = partialmethod(compare_model, "ReportCreated")

    compare_PrismPoint = partialmethod(compare_model, "PrismPoint")

    compare_Measurement = partialmethod(compare_model, "Measurement")

    compare_Section = partialmethod(compare_model, "Section")

    compare_Monograph = partialmethod(compare_model, "Monograph")

    compare_SpreadSheetReview = partialmethod(compare_model, "SpreadSheetReview")

    def get_compare_method(self, field):
        return getattr(self, f"compare_{field}")


@click.command()
@click.option("--model",
              type=str,
              help="'<model-class>', e.g. 'Section', 'Measurement'")
@click.option("--datafile",
              type=click.File('r'),
              help="filepath to read as csv")
@click.option("--delimiter",
              default=";",
              type=str,
              help="Delimiter of CSV data file")
def command(model, datafile, delimiter, test):

    compare_objects = CompareModelObjects()

    model_class = compare_objects.MODELS[model]
    compare_func = compare_objects.get_compare_method(model)
    fields = compare_objects.MODELS_FIELDS[model]

    users_ok = list()
    users_not_ok = list()

    reader = csv.DictReader(datafile, delimiter=delimiter)
    for row, elem in enumerate(reader):
        values = tuple(elem.get(field) for field in fields)
        values_dict = {field: elem.get(field) for field in fields}
        objects = model_class.objects.filter(**values_dict)
        if len(objects) == 0:
            users_not_ok.append(f"  No existe un objeto con los valores de la fila '{row}'.")

    if len(users_not_ok) == 0:
        print("  EXITO!")
        print("   Todas las filas del archivo CSV corresponden a un usuario")
        print("     y las columnas tienen valores identicos a los campos de la BD.")
    else:
        print(f"  HAY {len(users_not_ok)} ERRORES")
        for message in users_not_ok:
            print(message)
