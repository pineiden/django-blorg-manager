from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import (User, Group, Permission)
from pathlib import Path
from django.conf import settings
from datetime import datetime, time
import pytz
import random
from django.core.files.base import File
from pprint import pprint
from decimal import Decimal
from django.core.cache import cache

if settings.IS_MODULE:
    from topoapi.apps.base.models import BaseModel
    from topoapi.apps.axis.models import Axis
    from topoapi.apps.section.models import Section
    from topoapi.apps.prism_point.models import PrismPoint
    from topoapi.apps.file_manager.models import TopoFile, TopoFileReview
    from topoapi.apps.spreadsheet_review.models import SpreadsheetReview
    from topoapi.functions.displacements_calculator import DisplacementsCalculator
    from topoapi.functions.code import point_code_to_asp
    from topoapi.apps.prism_point.models import Measurement
else:
    from apps.base.models import BaseModel
    from apps.axis.models import Axis
    from apps.section.models import Section
    from apps.prism_point.models import PrismPoint
    from apps.file_manager.models import TopoFile, TopoFileReview
    from apps.spreadsheet_review.models import SpreadsheetReview
    from functions.displacements_calculator import DisplacementsCalculator
    from apps.prism_point.models import Measurement
    from functions.code import point_code_to_asp


class Command(BaseCommand):
    help = "Create data for topoapi"

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **kwargs):
        groups = ["terreno", "analista", "administrador"]
        users = ["geomensor", "analista", "admin"]
        assignation = dict(zip(users, groups))
        self.create_groups(groups=groups)
        self.creators = self.create_users(users=users, assignation=assignation)
        axes = ["01"]
        self.axes = self.create_axis(axes=axes)
        sections = [("1001", 44.08111), ("1002", 134.0811), ("1003", 224.07651), ("1004", 314.0811)]
        self.sections = self.create_sections(sections=sections)
        self.create_prism_points()

        # sp_path="/".join([this_path, "examples","review1.xlsx"])
        if settings.IS_MODULE:
            print(settings.BASE_DIR)
            names = ["topofile_cuad1.csv"]
            this_path = str(Path(settings.BASE_DIR).resolve())
            topo_files = list(
                map(lambda name: "/".join((this_path, "media", name)), names))
            self.topofiles, self.topofiles_review = self.create_topofiles(
                topo_files)
            sp_path = "/".join([this_path, "media", "review_cuadrantes.xlsx"])
        else:
            names = ["topofile_cuad1.csv"]
            this_path = str(Path(__file__).resolve().parent)
            topo_files = list(
                map(lambda name: "/".join((this_path, "examples", name)), names))
            self.topofiles, self.topofiles_review = self.create_topofiles(
                topo_files)
            sp_path = "/".join([this_path, "examples", "review_cuadrantes.xlsx"])
        # self.create_spreadsheet(sp_path)

    def create_groups(self, *args, **kwargs):
        group_names = kwargs.get("groups")
        for group in group_names:
            group, created = Group.objects.get_or_create(name=group)
            group.permissions.add(*Permission.objects.all())

    def create_users(self, *args, **kwargs):
        groups_assignation = kwargs.get("assignation", {})
        instances = {}
        for user in kwargs.get("users", []):
            user_instance = User.objects.create_user(
                username=user,
                password=user,
                email=f"{user}@geosinergia.cl",
                is_staff=True,
                is_active=True,
                is_superuser=(user == "admin")
            )
            print("User instance", user_instance)
            group = groups_assignation.get(user)
            group_instance = Group.objects.filter(name=group).first()
            if group_instance:
                user_instance.groups.add(group_instance)
            instances[user] = user_instance
        return instances

    def create_axis(self, *args, **kwargs):
        axes = kwargs.get("axes")
        instances = []
        for axis in axes:
            axis = Axis.objects.create(
                short_code=axis,
                name=f'eje {axis}',
                excavation_method='NATM',
                creator=self.creators.get("analista")
            )
            instances.append(axis)
        return instances

    def create_sections(self, *args, **kwargs):
        sections = kwargs.get("sections")
        instances = []
        for axis in self.axes:
            x = 0
            for section, azimuth in sections:
                x += random.uniform(0.1, 3)
                section_instance = Section.objects.create(
                    section_type='a',
                    short_code=section,
                    axis=axis,
                    excavation_distance=x,
                    section_parameters={},
                    azimuth=azimuth,
                    creator=self.creators.get("analista")
                )
                instances.append(section_instance)
        return instances

    def create_prism_points(self, *args, **kwargs):
        short_codes = list(map(str, [1]))
        for section in self.sections:
            for sh_code in short_codes:
                pp_instance = PrismPoint.objects.create(
                    short_code=sh_code,
                    active=True,
                    section=section,
                    creator=self.creators.get("analista")
                )

    def create_topofiles(self, topofile_list):
        topofiles = []
        topofiles_review = []
        for filepath in topofile_list:
            print("New topofile->", filepath)
            if Path(filepath).exists():
                topofile = TopoFile(
                    instrument="L",
                    creator=self.creators.get("geomensor"),
                )
                topofile.datafile = filepath
                topofile.save()
                topofiles.append(topofile)
                topofilereview = TopoFileReview.objects.create(
                    topofile=topofile,
                    status="1",
                    comment="holi",
                    creator=self.creators.get("analista"))
                topofiles_review.append(topofilereview)

        return topofiles, topofiles_review

    def create_spreadsheet(self, filepath):
        if Path(filepath).exists():
            instance = SpreadsheetReview(
                creator=self.creators.get("analista"),
                topofile_review=self.topofiles_review[0],
            )
            instance.spreadsheet = filepath
            instance.save()
            key_name = f"measurement_{instance.id}"
            print("Rescue key data", key_name)
            data = cache.get(key_name)
            test = cache.get("test")
            print(f"TEST {test}, keys {data.keys()}")
            pprint('///////////////////////////////////////////////////////')
            pprint('///////////////////////////////////////////////////////')
            # pprint(instance.validation_report_json)
            pprint('///////////////////////////////////////////////////////')
            pprint('///////////////////////////////////////////////////////')

            sections = dict()
            for section_instance in Section.objects.all():
                sections[section_instance.code] = section_instance
                print("saving section:", section_instance)
            point_instances = dict()
            print("="*40)
            print("Data keys <",data.keys(),">")
            print("="*40)
            # for point_code, values in data.items():
            #     for values_dict in values:
            #         code = values_dict.get('POINT_CODE')
            #         print(code)

            #         axis_code, section_short_code, point_short_code = point_code_to_asp(
            #             code)
            #         print("CODES", axis_code,
                    #       section_short_code, point_short_code)
                    # section_code = settings.CODE_SEPARATOR.join([
                    #     axis_code, section_short_code])
                    # print("SECTION CODE", section_code, sections.keys())
                    # if section_code in sections:
                    #     print(sections.get(section_code).code)
                    #     print(sections.get(section_code).short_code)
                    #     print(sections.get(section_code).axis.code)

                    #     section_instance = sections.get(section_code)
                    #     azimuth = section_instance.azimuth
                    #     prism_point = point_instances.get(code)

                    #     if prism_point is None:
                    #         prism_point = PrismPoint.objects.get(short_code=point_short_code,
                    #                                              section=section_instance)
                    #         point_instances[code] = prism_point

                    #     local_timezone = pytz.timezone("America/Santiago")
                    #     hora = values_dict.get("TIME")
                    #     if isinstance(hora, datetime):
                    #         hora = hora.time()
                    #     fecha_hora = datetime.combine(
                    #         values_dict.get("DATE"), hora)
                    #     local_fh = local_timezone.localize(fecha_hora)

                    #     pprint("medida ok->")
                    #     mes=Measurement.objects.create(
                    #         prism_point=prism_point,
                    #         status=values_dict.get("STATUS"),
                    #         creator=self.creators.get('analista'),
                    #         east=Decimal(values_dict.get("EAST")),
                    #         north=Decimal(values_dict.get("NORTH")),
                    #         up=Decimal(values_dict.get("ELEVATION")),
                    #         dt_gen=local_fh,
                    #         delta_x=Decimal(values_dict.get("DELTA_X")),
                    #         delta_y=Decimal(values_dict.get("DELTA_Y")),
                    #         delta_z=Decimal(values_dict.get("DELTA_Z")),
                    #     )
                    #     print(mes)
