from django.contrib.auth.models import User
import djclick as click


@click.command()
@click.option("--all_users", type=bool, default=True, help="If list all users or not")
@click.option("--username", type=str, default="", help="""If different to empty string, check usernames that contains 'username'""")
@click.option("--groups", type=bool, default=True, help="If show groups by user")
def command(all_users, username, groups):
    users = User.objects.none()
    if all_users:
        if username:
            users = User.objects.filter(username__contains=username)
        else:
            users = User.objects.all()
    for user in users:
        print(user.id, user.username, user.email)
        if groups:
            for group in user.groups.all():
                print("->", group)
