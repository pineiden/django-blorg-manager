from django.conf import settings
from django.contrib.auth.models import User
from datetime import datetime
from pathlib import Path
from pprint import pprint
from collections import OrderedDict
from os import environ
if settings.IS_MODULE:
    from topoapi.apps.axis.models import (Axis, ExcavationPhase,
                                          ExcavationFrontProgress)
    from topoapi.apps.file_manager.models import TopoFile
    from topoapi.apps.pdf_report.models import ReportCreated
    from topoapi.apps.note.models import Note
    from topoapi.apps.pdf_report.models import ReportCreated
    from topoapi.apps.prism_point.models import PrismPoint, Measurement
    from topoapi.apps.section.models import Section, Monograph
    from topoapi.functions.checksum import checksum, checksum_inmemory_value
else:
    from apps.axis.models import (Axis, ExcavationPhase,
                                  ExcavationFrontProgress)
    from apps.note.models import Note
    from apps.pdf_report.models import ReportCreated
    from apps.prism_point.models import PrismPoint, Measurement
    from apps.section.models import Section, Monograph
    from apps.file_manager.models import TopoFile
    from apps.pdf_report.models import ReportCreated
    from functions.checksum import checksum, checksum_inmemory_value
import djclick as click
import csv

FLOAT_TOLERANCE = 1.e-6
TIME_FIELD_TOLERANCE_SECONDS = 0.5

MODELS = {
    "User": {
        "class": User,
        "fields": (("username", "str"),
                   ("email", "str"),
                   ("first_name", "str"),
                   ("last_name", "str"),
                   ("password", "str"))

    },
    # axis ----
    "Axis": {
        "class": Axis,
        "fields": (("short_code", "str"),
                   ("name", "str"),
                   ("axis_type", "str"),
                   ("excavation_method", "str"),
                   # ("thresholds", "json"),
                   ("is_horizontal", "bool"),
                   ("has_free_points", "bool"))
    },

    "ExcavationPhase": {
        "class": ExcavationPhase,
        "fields": (("code", "str"),
                   ("name", "str"),
                   ("description", "str"),
                   ("is_horizontal", "bool"))
    },

    "ExcavationFrontProgress": {
        "class": ExcavationFrontProgress,
        "fields": (("axis", "axis_field"),
                   ("excavated_distance", "float"),
                   ("datetime", "datetime"),
                   ("phase", "excavation_phase_field"),
                   ("comment", "str"))
    },

    # file_manager ----
    "TopoFile": {
        "class": TopoFile,
        "fields": (("datafile", "file"),
                   ("instrument", "str"),
                   ("status", "str"))
    },
    # note ----
    "Note": {
        "class": Note,
        "fields": (("attach_to", "section_field"),
                   ("text", "str"),
                   ("is_longitudinal", "bool"))
    },
    # prism_point ----
    "PrismPoint": {
        "class": PrismPoint,
        "fields": (("code", "property"),
                   ("active", "bool"),
                   ("is_trigonometric", "bool"),
                   ("is_free", "bool"),
                   ("azimuth", "float"))
    },
    "Measurement": {
        "class": Measurement,
        "fields": (("prism_point", "prism_point_field"),
                   ("dt_gen", "datetime"),
                   ("is_fixed", "bool"),
                   ("east", "float"),
                   ("north", "float"),
                   ("up", "float"),
                   ("status", "int"),
                   ("comment", "str"),
                   ("delta_x", "float"),
                   ("delta_y", "float"),
                   ("delta_z", "float"))
    },
    # section ----
    "Section": {
        "class": Section,
        "fields": (("short_code", "str"),
                   ("section_type", "str"),
                   ("axis", "axis_field"),
                   ("excavation_distance", "float"),
                   ("project_kilometer", "str"),
                   ("azimuth", "float"))
    },
    "Monograph": {
        "class": Monograph,
        "fields": (("section", "section_field"),
                   ("figure", "image"),
                   ("comment", "str"))
    },
    # pdf_report ----
    "ReportCreated": {
        "class": ReportCreated,
        "fields": (("pdf", "file"),
                   ("public", "bool"))
    },
}

EQUAL_QUERY_TYPES = ("int", "str", "bool", "datetime")


class CompareModelObjects(object):

    @staticmethod
    def to_bool(field_str):
        return field_str in ("true", "True", 1)

    @staticmethod
    def to_int(field_str):
        return int(field_str)

    @staticmethod
    def to_str(field_str):
        return field_str

    @staticmethod
    def to_float(field_str):
        return float(field_str)

    @staticmethod
    def to_datetime(datetime_str):
        return datetime.fromisoformat(datetime_str)

    to_file = to_str

    @staticmethod
    def parse_csv_row_dict(model, str_dict):
        equal_query_dict = {}
        other_values_dict = {}
        for field, field_type in MODELS[model]["fields"]:
            if field in str_dict:
                if field_type[-6:] == "_field":  # model type
                    value = str_dict[field]
                elif field_type == "property":  # property type
                    value = str_dict[field]
                else:
                    value = getattr(CompareModelObjects,
                                    f"to_{field_type}")(str_dict[field])
                if field_type in EQUAL_QUERY_TYPES:
                    equal_query_dict[field] = value
                else:
                    other_values_dict[field] = (field_type, value)
        return equal_query_dict, other_values_dict

    @staticmethod
    def get_csv_dict_parser(model_class):
        def parser_func(str_dict):
            return CompareModelObjects.parse_csv_row_dict(model_class, str_dict)
        return parser_func

    @staticmethod
    def equals_compare(object, field, value):
        return getattr(object, field) == value

    compare_str = equals_compare

    compare_int = equals_compare

    compare_bool = equals_compare

    @staticmethod
    def compare_float(object, field, value):
        return abs(float(getattr(object, field))-value) < FLOAT_TOLERANCE

    @staticmethod
    def compare_datetime(object, field, datetime_value):
        delta_t = (getattr(object, field).timestamp()
                   - datetime_value.timestamp())
        return abs(delta_t) < TIME_FIELD_TOLERANCE_SECONDS

    @staticmethod
    def compare_email(object, field, email_str):

        try:
            username = object.username
            if email_str.startswith("@"):
                email = f"{username}{email_str}"
            else:
                email = email_str
        except AttributeError:
            email = email_str
        return getattr(object, field) == email

    @staticmethod
    def compare_file(object, field, file_name):
        dir_path = Path(environ.get("PATH_DIR_AUX"), "")
        file_path = dir_path / file_name
        checksum_ok = (checksum_inmemory_value(
            getattr(object, field)) == checksum(file_path))
        if not checksum_ok:
            return False
        # equal = True
        with open(file_path, "r") as reader_local:
            with getattr(object, field).open("r") as reader:
                # equal = all(line_loc == line for line_loc, line
                #             in zip(reader_local, reader))
                for line_loc, line in zip(reader_local, reader):
                    if line_loc != line.decode():
                        print(" LINEAS DIFERENTES !!")
                        print(line_loc)
                        print(line)
                        return False
        # return equal
        return True

    compare_user_field = equals_compare

    compare_property = equals_compare

    @staticmethod
    def compare_axis_field(object, field, axis_code):
        """
        :param object: table entry
        :param field: object.<field> is a Section instance
        :param axis_code: axis code "<axis>"
        """
        return getattr(object, field).short_code == axis_code

    @staticmethod
    def compare_excavation_phase_field(object, field, excavation_phase_code):
        return getattr(object, field).code == excavation_phase_code

    @staticmethod
    def compare_prism_point_field(object, field, prism_point_code):
        """
        :param object: table entry
        :param field: object.<field> is a PrismPoint instance
        :param section_code: section code "<axis>_<section>_<point>"
        """
        return getattr(object, field).code == prism_point_code

    @staticmethod
    def compare_section_field(object, section_code):
        """
        :param object: table entry
        :param field: object.<field> is a Section instance
        :param section_code: section code "<axis>_<section>"
        """
        return object.code == section_code

    @staticmethod
    def get_compare_method(field):
        return getattr(CompareModelObjects, f"compare_{field}")



@click.command()
@click.option("--model",
              type=str,
              help="'<model-class>', e.g. 'Section', 'Measurement'")
@click.option("--dir_path",
              type=str,
              default="",
              help="filepath where the CSV file is")
@click.option("--csv_file",
              type=str,
              help="CSV file name")
@click.option("--delimiter",
              default=";",
              type=str,
              help="Delimiter of CSV data file")
def command(model, dir_path, csv_file, delimiter):

    if len(dir_path) > 0:
        environ["PATH_DIR_AUX"] = dir_path
    compare_objects = CompareModelObjects()

    model_class = MODELS[model]["class"]
    row_parser = CompareModelObjects.get_csv_dict_parser(model)
    entries_not_ok = list()
    d_path = Path(dir_path)
    with open(d_path / csv_file, newline='') as csv_f:
        reader = csv.DictReader(csv_f, delimiter=delimiter)
        for row, str_dict in enumerate(reader):
            equal_query_dict, non_equal_query_dict = row_parser(str_dict)

            objects = model_class.objects.filter(**equal_query_dict)
            if len(objects) == 0:
                entries_not_ok.append((row, str_dict))
            else:
                ok = any(
                    all(compare_objects.get_compare_method(f_type)(
                        obj, field, value) for (field, (f_type, value))
                        in non_equal_query_dict.items())
                    for obj in objects)
                if not ok:
                    entries_not_ok.append((row, str_dict))
        n_err = len(entries_not_ok)
        if n_err == 0:
            print("  EXITO!")
            print("     Todas y cada una de las filas del archivo CSV corresponden")
            print(f"   a entradas en la tabla {model}. Es decir, las valores en el")
            print("   CSV son idénticos a los campos de dicha tabla.")
        else:
            print(f"  Las siguientes filas del csv no corresponden a entradas en la tabla {model}")
            for row, str_dict in entries_not_ok:
                print(f"  Fila {row+1}:")
                if isinstance(str_dict, OrderedDict):
                    str_dict = dict(str_dict)
                pprint(str_dict)
