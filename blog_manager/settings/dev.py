from .base import *
from .rest import REST_FRAMEWORK
from .cache import CACHES
from .celery import *
from .blogs import (BLOGS_DIR, GIT_TIMEOUT, GIT_UPDATE_CRONTAB,
                    GIT_CLONE_DEPTH, BLOGS_REGEX)

# enable debug
DEBUG = True

# Add to installed modules to dev 
INSTALLED_APPS.append('djecrety')

# Database
# https://docs.djangoproject.com/en/4.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    }
}
