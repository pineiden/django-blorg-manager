from .base import BASE_DIR
import yaml
from pathlib import Path
import copy
import re

BLOGS_DIR = BASE_DIR / "blogs"
GIT_TIMEOUT = 20
GIT_CLONE_DEPTH = 1
GIT_UPDATE_CRONTAB = {
    "minute": "*/10",
    "hour": "*",
    "day_of_week": "*",
    "month_of_year": "*",
}

BLOGS_REGEX = r'^(\w+\d*\.)+html$'

CONFIG_FILE = Path(__file__).parent / "config.yml"
CONFIG_TEXT = CONFIG_FILE.read_text()
DEFAULT_CONFIG_REPO = yaml.load(
    CONFIG_TEXT, 
    Loader=yaml.FullLoader)

def config_expand(data):
    data = copy.deepcopy(data)
    items_menu = data.get("menu", {}).get("items",{})
    for key, elem in items_menu.items():
        position = elem["position"]
        if position == "first":
            elem["position"] = 0
        elif position == "last":
            elem["position"] = len(items_menu)-1
        elif position.isdigit():
            elem["position"] = int(position)
    for key, elem in items_menu.items():
        position = elem["position"]
        if isinstance(position, str):
            if position.startswith("right-to:"):
                print("THIS ELEM PRE", elem, position)
                el_key = position.replace("right-to:","")
                pos = items_menu[el_key]["position"]
                elem["position"] = int(pos)+1
                print("THIS ELEM POS", elem, position)
            elif position.startswith("left-to:"):
                el_key = position.replace("left-to:","")
                pos = items_menu[el_key]["position"]
                elem["position"] = int(pos)-1
    return data

CONFIG_REPO_EXPANDED = config_expand(DEFAULT_CONFIG_REPO)
