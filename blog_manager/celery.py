import os

from celery import Celery

mode = os.environ.get("MODE")
project_name = os.environ.get("PROJECT", "blog_manager")
# Set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', f'{project_name}.settings.{mode}')

app = Celery(project_name)

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django apps.
app.autodiscover_tasks()

celery_app  = app
