from rest_framework import routers
from apps.repository.rest.routers import repository_route_list
import itertools

router = routers.DefaultRouter()

rlist = [
    repository_route_list
]

router_list = list(itertools.chain.from_iterable(rlist))

for name, viewset in router_list:
    router.register(name, viewset)
