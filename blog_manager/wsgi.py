"""
WSGI config for blog_manager project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/4.0/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

MODE = os.environ.get('SECRET_KEY')

os.environ.setdefault('DJANGO_SETTINGS_MODULE', f'blog_manager.settings.{MODE}')

application = get_wsgi_application()
