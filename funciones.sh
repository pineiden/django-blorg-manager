flower_blorg(){
celery -A blog_manager flower --host=0.0.0.0 --port=5555
#flower -A topoapi --port=5555
}

beat_blorg(){
celery -A blog_manager beat -l info --loglevel=INFO -S django
}


worker_blorg(){
celery -A blog_manager worker -l info
}

echo flower_blorg
echo beat_blorg
echo worker_blorg


pyshell () {
./manage.py shell_plus
}

runserver(){
./manage.py runserver
}

echo pyshell
echo runserver


dotest () {
echo "test for : $@"
./manage.py test $1 
}

echo dotest
