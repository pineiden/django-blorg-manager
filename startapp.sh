# OPTIONS >
# 
# -n :: app name, obligatory
# -t :: create templates folder {0,1}
# -s :: create static folder {0,1}
# -m :: create media folder {0,1}
# -u :: create urls.py file {0,1}
# -i :: define project name o main folder
#
# how to run?
#
# COMMAND ./startapp.sh -n NAME -t 1 -s 0 -r 1 -c 1-i mi_web
#

project_name=$(pwd|awk -F'/' '{print $NF}')
templates=1
static=1
media=0
urls=1
rest=0
channels=0
DEBUG=1

while getopts n:t:s:m:u:i:d:r:c: option
do
case "${option}"
in
  n) app=${OPTARG};;
  t) templates=${OPTARG};;
  s) static=${OPTARG};;
  m) media=${OPTARG};;
  u) urls=${OPTARG};;
  i) project_name=${OPTARG};;
  d) DEBUG=${OPTARG};;
  r) rest=${OPTARG};;
  c) channels=${OPTARG};;
esac
done


echo "DEBUG FLAG : <${DEBUG}>"
echo "Creando apps en ${project_name}"
install_file="./${project_name}/settings/installed.py"

echo "Creating <"$app"> folder"

if [ $DEBUG -eq 0 ]
then
    mkdir ./apps/$app
fi

echo "Creating app inside the folder <./apps/"$app">"

if [ $DEBUG -eq 0 ]
then
    python manage.py startapp $app ./apps/$app
fi


echo "Template value <${templates}>"
if [ $templates -eq 1 ]
then
    echo "Creating templates"
    if [ $DEBUG -eq 0 ]
    then
        mkdir ./apps/$app/templates
        mkdir ./apps/$app/templates/$app
    fi
fi

echo "Static creation value <${static}>"
if [ $static -eq 1 ]
then
    echo "Creating static folder"
    if [ $DEBUG -eq 0 ]
    then
        mkdir ./apps/$app/static
        mkdir ./apps/$app/static/$app
    fi
fi

echo "Media creation value <${media}>"
if [ $media -eq 1 ]
then
    echo "Creating media folder"
    if [ $DEBUG -eq 0 ]
    then
        mkdir ./apps/$app/media
    fi
fi


echo "URLS creation value <${urls}>"
if [ $urls -eq 1 ]
then
    echo "Creating urls file"
    if [ $DEBUG -eq 0 ]
    then
        touch ./apps/$app/urls.py
    fi
fi

echo "Agregando app a INSTALLED_APPS -> apps.${app}"


if [ $DEBUG -eq 0 ]
then
    sed -i "/]/i\    \"apps."$app"\"," $install_file
fi



if [ $rest -eq 1 ]
then
    echo "Creating rest folder"
    if [ $DEBUG -eq 0 ]
    then
        dir="./apps/$app/rest"
        mkdir $dir
        touch $dir/serializers.py
        echo "from rest_framework import serializers" >  $dir/serializers.py
        echo "from ..models import ()">>  $dir/serializers.py
        touch $dir/viewsets.py
        echo "from rest_framework import viewsets" > $dir/viewsets.py
        echo "from .serializers import ()" >> $dir/viewsets.py
        touch $dir/routers.py
        echo "from rest_framework import routers" >  $dir/routers.py
        echo "from .viewsets import ()" >>  $dir/routers.py
        echo "router = routers.DefaultRouter()" >>  $dir/routers.py
    fi
fi


if [ $channels -eq 1 ]
then
    echo "Creating rest folder"
    if [ $DEBUG -eq 0 ]
    then
        dir="./apps/$app/channels"
        mkdir $dir
        touch $dir/consumers.py
        touch $dir/routing.py
    fi 
fi






