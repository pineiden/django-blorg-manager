echo "Param 1 "$1
USER_WEB=$1
echo "El usuario es "$USER_WEB
DIR=$(python3 web_path.py)
echo "Directorio de trabajo"$DIR
cd $DIR
env_name="web"
source /home/$USER_WEB/.variables_ambiente
echo "Parametros de Ambiente cargados"
echo "Cargando virtualenvwrapper:::::"
workon $env_name
echo "modo de operación-> "$MODO
path_sock="/home/web/sockets/web.sock"
echo "Path to socket" $path_sock
uvicorn topoapi.asgi:application --uds $path_sock --reload --workers 4 --log-level info --access-log --use-colors --ws websockets --debug
